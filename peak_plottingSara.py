import h5py
import numpy as np
import logging as log
import argparse
import os
import matplotlib.pyplot as plt
from mpl_toolkits.axes_grid1 import make_axes_locatable
import matplotlib
from sklearn.metrics import roc_curve, auc, roc_auc_score
from scipy import interpolate

from utils import mkdir
from itertools import combinations
from skhep.math import vectors
import multiprocessing

import pandas as pd

from scipy.special import logit

from peakfit import PeakFit_likelihood

"""
Here you alter the data needed to be fitted
This is then send to the script peakfitSara.py to do the fit.

"""

data = pd.read_pickle("/groups/hep/sda/work/MastersThesis/Zmmg/Zmmg/output/mumugamPredictions/20201110/pred_data.pkl")

data["predLGBM"] = logit(data["predLGBM"])
# residues = []
# nsigs = []
# nbkgs = []
# ntotals = []
SigEff = []
BkgEff = []

cut_vals = []
bkgRatio_ATLAS = 0.00023890336796182296

# cuts = np.linspace(6,8, num = 50)
cuts = np.linspace(10,20, num = 100)
# cuts = np.linspace(2,3, num = 60)
#cuts = [7.09]
# cuts = [-40]
i = 1
j = 1
# for cutval in cuts:
cutval = -50
for x in [1]:
    Likelihood_cut = data["isATLAS"]
    # Likelihood_cut = (data["predLGBM"] > cutval)
    BL_sig, BL_bkg, sig_ratio, bkg_ratio = PeakFit_likelihood(Likelihood_cut*1, data["invM"], "ATLAS cut", plots = True, constant_mean = True,
                                           constant_width = True, classifier_name = 'Likelihood', CB = True, Gauss = False, bkg_comb = False,
                                           bkg_exp = True, bkg_cheb = False);
    print(f"The bkg_ratio is {bkg_ratio}")
#     if BL_sig != 0:
#         SigEff.append(sig_ratio)
#         BkgEff.append(bkg_ratio)
#
#         if (bkg_ratio < bkgRatio_ATLAS + 0.0005) & (bkg_ratio > bkgRatio_ATLAS - 0.0005):
#             print(f"Found correct background efficiency for a cut at {bkg_ratio} compared to true {bkgRatio_ATLAS}")
#             print(f"My cut is {cutval}")
#             print(f"Breaking....")
#
#             break
# print("Signal Efficiencies:")
# print(SigEff)
# print("Background Efficiencies:")
# print(BkgEff)
