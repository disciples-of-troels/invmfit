from tqdm import tqdm

import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from scipy.stats import crystalball
from skhep.math import vectors
from invmfit import invmfit

def invMass(tag, probe, third = None):
    # Calculate mZee using: https://github.com/scikit-hep/scikit-hep/blob/master/skhep/math/vectors.py?fbclid=IwAR3C0qnNlxKx-RhGjwo1c1FeZEpWbYqFrNmEqMv5iE-ibyPw_xEqmDYgRpc
    # Get variables
    p1 = tag['ele_p_T']
    eta1 = tag['ele_truth_eta_egam']
    phi1 = tag['ele_truth_phi_egam']
    p2 = probe['ele_p_T']
    eta2 = probe['ele_truth_eta_egam']
    phi2 = probe['ele_truth_phi_egam']
    
    # make four vector
    vecFour1 = vectors.LorentzVector()
    vecFour2 = vectors.LorentzVector()
    
    vecFour1.setptetaphim(p1, eta1, phi1, 0.000511) #Units in GeV for pt and mass
    vecFour2.setptetaphim(p2, eta2, phi2, 0.000511)
    vecFour = vecFour1+vecFour2
        
    invM = vecFour.mass
    pt = vecFour.et
    eta = vecFour.eta
    phi = vecFour.phi()
    return invM, pt, eta, phi

df = pd.read_pickle('./data/MC_test_Zee.pkl')

Z_m = []
for evtnr in tqdm(df.index.drop_duplicates()):
    # print(evtnr)
    values = df[df.index == evtnr]

    output = invMass(tag = values.iloc[0,:], probe= values.iloc[1,:])
    Z_m.append(output[0])


value = invmfit(np.ones(len(Z_m)), pd.array(Z_m),
                            plots = True, constant_mean = False,
                            constant_width = False,
                            classifier_name = 'Truth',
                            CB = True)


