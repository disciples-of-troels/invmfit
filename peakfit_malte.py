from tqdm import tqdm

import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from scipy.stats import crystalball
from skhep.math import vectors
import sys
# insert at 1, 0 is the script path (or '' in REPL)
sys.path.insert(1, '../hdf5Prod')
from from_event2row import h5_to_pkl


def invMass(tag, probe, third = None):
    # Calculate mZee using: https://github.com/scikit-hep/scikit-hep/blob/master/skhep/math/vectors.py?fbclid=IwAR3C0qnNlxKx-RhGjwo1c1FeZEpWbYqFrNmEqMv5iE-ibyPw_xEqmDYgRpc
    # Get variables
    p1 = tag['ele_p_T']
    eta1 = tag['ele_truth_eta_egam']
    phi1 = tag['ele_truth_phi_egam']
    p2 = probe['ele_p_T']
    eta2 = probe['ele_truth_eta_egam']
    phi2 = probe['ele_truth_phi_egam']
    # make four vector
    vecFour1 = vectors.LorentzVector()
    vecFour2 = vectors.LorentzVector()
    
    vecFour1.setptetaphim(p1, eta1, phi1, 0.000511) #Units in GeV for pt and mass
    vecFour2.setptetaphim(p2, eta2, phi2, 0.000511)
    # calculate invariant mass
    if third is not None:
        p3 = third['pho_p_T']
        eta3 = third['pho_truth_eta_egam']
        phi3 = third['pho_truth_phi_egam']
        # E3 = third['pho_truth_E_egam']
        vecFour3 = vectors.LorentzVector()
        mass = np.sqrt(third['pho_truth_E_egam']**2-
                       third['pho_truth_px_egam']**2-
                       third['pho_truth_py_egam']**2-
                       third['pho_truth_pz_egam']**2)
        vecFour3.setptetaphim(p3, eta3, phi3, 0)
        # px = third['pho_truth_px_egam']
        # py = third['pho_truth_py_egam']
        # pz = third['pho_truth_pz_egam']
        # vecFour3 = vectors.LorentzVector()
        # vecFour3.setpxpypze(px,py,pz,E)
        vecFour = vecFour1+vecFour2+vecFour3 
    else:
        vecFour = vecFour1+vecFour2
        
    invM = vecFour.mass
    pt = vecFour.et
    eta = vecFour.eta
    phi = vecFour.phi()
    return invM, pt, eta, phi

columns =  ['ele_truth_phi_egam', 'ele_truth_eta_egam', 'ele_truth_pz_egam', 'ele_truth_px_egam',
            'pho_truth_pz_egam','ele_truth_py_egam',
            'pho_truth_phi_egam', 'pho_truth_eta_egam',
            'ele_truthOrigin', 'pho_truthOrigin',
            'ele_truthPdgId_egam', 'pho_truthPdgId_egam', 'pho_truth_px_egam', 'pho_truth_py_egam',
            'pho_truth_E_egam', 'pho_truth_m_egam', 'ele_truth_E_egam']

# columns =  ['ele_truth_phi_egam', 'ele_truth_eta_egam', 'ele_truth_pz_egam',
#             'ele_truthOrigin','ele_truthPdgId_egam']

mypath = '../hdf5Prod/output/root2hdf5/Zee'
onlyfiles = 'Zee_0000.h5'
df = h5_to_pkl(mypath+'/'+onlyfiles, columns = columns)
df.to_pickle('MC_test_Zee.pkl')
df['ele_p_T'] = df['ele_truth_pz_egam']/np.sinh(df['ele_truth_eta_egam'])

monte_carlo = True
if (np.array(mypath.split('/')) == 'Zee').any() & monte_carlo: ## for Zee
    df = df[(df['ele_truthPdgId_egam'].abs() == 11) & (df['ele_truthOrigin'] == 13)]
    mask_double = np.array([np.sum(df.index == i) == 2 for i in df.index])
    df = df[mask_double]
    
elif (np.array(mypath.split('/')) == 'Zllgam').any() & monte_carlo: ## for Zee
    df['pho_p_T'] = df['pho_truth_pz_egam']/np.sinh(df['pho_truth_eta_egam'])
    # df = df[(df['ele_truthPdgId_egam'].abs() == 11) & (df['ele_truthOrigin'] == 13)]
    mask_triple = np.array([np.sum(df.index == i) == 3 for i in df.index])
    df = df[mask_triple]
#%%
Z_m = []
Z_m_single = []
rapidity = []
for evtnr in tqdm(df.index.drop_duplicates()):
    # print(evtnr)
    values = df[df.index == evtnr]
    if len(values) == 3:
        third = values[values['ele_truthPdgId_egam'].abs() != 11]
        tag_probe = values[values['ele_truthPdgId_egam'].abs() ==11]
        if len(third) >= 2:
            Z_m.append(0)
            Z_m.append(0)
            Z_m.append(0)
            continue
        if third['pho_truth_E_egam'].isna().values[0]:
            mass_squared = ((tag_probe['ele_truth_E_egam'].sum()+third['ele_truth_E_egam'])**2-
                    (tag_probe['ele_truth_px_egam'].sum()+third['ele_truth_px_egam'])**2-
                    (tag_probe['ele_truth_py_egam'].sum()+third['ele_truth_py_egam'])**2-
                    (tag_probe['ele_truth_pz_egam'].sum()+third['ele_truth_py_egam'])**2)
        else:
            mass_squared = ((tag_probe['ele_truth_E_egam'].sum()+third['pho_truth_E_egam'])**2-
                    (tag_probe['ele_truth_px_egam'].sum()+third['pho_truth_px_egam'])**2-
                    (tag_probe['ele_truth_py_egam'].sum()+third['pho_truth_py_egam'])**2-
                    (tag_probe['ele_truth_pz_egam'].sum()+third['pho_truth_py_egam'])**2)
        mass = np.sqrt(mass_squared.values[0])
        # try:
        #     output = invMass(tag = tag_probe.iloc[0,:], probe= tag_probe.iloc[1,:], third = third.iloc[0,:])
        # except:
        #     Z_m.append(0)
        #     Z_m.append(0)
        #     Z_m.append(0)
        #     continue
    else:
        output = invMass(tag = values.iloc[0,:], probe= values.iloc[1,:])
    # print(output)
    Z_m.append(mass)
    Z_m.append(mass)
    Z_m.append(mass)
    Z_m_single.append(mass)

    # p_z = values['ele_truth_pz_egam'].sum() 
    # p_x = values['ele_truth_px_egam'].sum() 
    # p_y = values['ele_truth_py_egam'].sum() 
    # E = np.sqrt(p_x**2+p_y**2+p_z**2+output[0]**2)
    # rapidity.append(1/2*np.log((E+p_z)/(E-p_z)))
    # rapidity.append(1/2*np.log((E+p_z)/(E-p_z)))
df['Z_m'] = Z_m
# df['rapidity'] = rapidity

#%%

from peakfit import inv_peakfit
# plt.close('all')
if False:
    mask_truth = (df['Truth'] == 1) & (df['p_ECIDSResult'] > -0.33) & (df['p_charge']*df['tag_charge'] < 0)
    df = df[mask_truth]
CB = False
if True:
    if CB:
        change = crystalball.rvs(3, 4, size=len(df)) * 0 * 1000
    else:
        change = np.random.normal(0,1, size=len(df)) * 0 * 1000
    # df['Z_m_smeared'] = df['Z_m']+change
    value = inv_peakfit(pd.DataFrame(np.ones(len(Z_m))), pd.DataFrame(Z_m_single).dropna(),
                                plots = True, constant_mean = False,
                                constant_width = False,
                                classifier_name = 'Likelihood',
                                CB = True)
else:
    for k in range(1):
        mask_rapidity = df['rapidity'].abs() < 1
        values = []
        errors = []
        smear = []
        for i in tqdm(np.linspace(0, 10, 11, dtype = 'int')):
            if CB:
                change = crystalball.rvs(3, 4, size=len(df)) * i
            else:
                change = np.random.normal(0,1, size=len(df)) * i
            df['Z_m_smeared'] = df['Z_m']+change
            value = inv_peakfit(pd.DataFrame(np.ones(len(df[mask_rapidity]))), pd.DataFrame(df.Z_m_smeared[mask_rapidity].values),
                                        plots = True, constant_mean = False,
                                        constant_width = False,
                                        classifier_name = i,
                                        CB = True)
            if value == 'error':
                continue
            values.append(value.getVal())
            errors.append(value.getError())
            smear.append(i)
            print('what')
        print(smear, values, errors)
        # smear = [0, 2, 3, 4, 6, 7, 8, 9]
        # values = [1.7214175579098263, 2.787894176754192, 3.5628572870056194, 4.548203756799652, 6.419732167524371, 7.420964046803464, 8.519094449903172, 9.383313644708606]
        # errors = [0.016715197955106786, 0.015629443681916655, 0.009437930391940075, 0.02169967541838469, 0.03114047819024357, 0.024828003990159164, 1.8916422206416428e-08, 0.04177579071764814]
        # smear_CB = [0, 1, 2, 3, 6, 7, 8, 10]
        # values_CB = [1.7214175579098263, 2.0472270784099655, 2.7433141724052703, 3.632593244314329, 6.511252021703375, 7.398011107292353, 8.419499497552724, 9.999999986778244]
        # errors_CB = [0.016715197955106786, 0.01229398374393087, 0.017731297860260842, 0.019768977800384446, 0.04344550496565924, 0.01941938340460192, 0.011627996457325374, 0.0018512631288523096]
    
        plt.errorbar(smear, values, errors, label = 'Smearing E_e with Gaussian - |rapidity| < 1', color = 'red')
        # plt.errorbar(smear_CB, values_CB, errors_CB, label = 'Smearing E_e - Crystal ball', color = 'red')
        plt.plot(np.arange(11), np.arange(11), 'b--')
        plt.xlabel(r'$\sigma_{true}$')
        plt.ylabel(r'$\sigma_{fit}$')
        plt.legend()
        plt.title(r'Smearing $E_e$')
        plt.savefig('./figures/width_smearing.png')
        print('Saved!')
        print('what')






